# do this once
# devtools::install_bitbucket("remkoduursma/plotby")

#---------------------------------------------------------------------
#- function to load a package, and install it if necessary
Library <- function(pkg, ...){
  
  PACK <- .packages(all.available=TRUE)
  pkgc <- deparse(substitute(pkg))
  
  if(pkgc %in% PACK){
    library(pkgc, character.only=TRUE)
  } else {
    install.packages(pkgc, ...)
    library(pkgc, character.only=TRUE)
  }
  
}
#---------------------------------------------------------------------





#---------------------------------------------------------------------
#- load all the libraries (and install them if needed)
Library(doBy)
Library(magicaxis)
Library(RColorBrewer)
Library(propagate)
Library(gplots)
Library(readxl)
Library(maps)
Library(mapdata)
Library(rgeos)
Library(sp)
Library(raster)
Library(nlstools)
Library(rgl)
Library(mgcv)
Library(scales)
Library(data.table)
Library(dplyr)
Library(dismo)
Library(AICcmodavg)
Library(png)
Library(plantecophys)
Library(nlme)
Library(lme4)
Library(dplyr)
Library(plyr)
Library(broom)
Library(car)
Library(lubridate)
#---------------------------------------------------------------------


#---------------------------------------------------------------------
#---------------------------------------------------------------------

# load required functions

source("R/Functions/functions_for_analysis.R")
source("R/Functions/metdataprocess.R")
source("R/Functions/rsq_mm.R")
# source("functions.R")
# source("R/metdataprocess.R")
# source("R/rsq_mm.R")

#---------------------------------------------------------------------
#---------------------------------------------------------------------

# need some directories to create 

#----------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------

# store fitted parameters of Farquhar model for each dataset seperately

if(!dir.exists("ACI_FITS")){
  dir.create("ACI_FITS")
}
#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------

# store  ACi curve plots (pdf) for each dataset seperately


if(!dir.exists("ACI_PLOTS")){
  dir.create("ACI_PLOTS")
}

#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------

# store fitted temperature response parameters of photosynthesis, Vcmax, Jmax ect...

if(!dir.exists("Tables")){
  dir.create("Tables")
}

#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------

# store figures of the manuscript

if(!dir.exists("manuscript_figures")){
  dir.create("manuscript_figures")
}

#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------

# store tables of the manuscript

if(!dir.exists("manuscript_tables")){
  dir.create("manuscript_tables")
}

#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------
