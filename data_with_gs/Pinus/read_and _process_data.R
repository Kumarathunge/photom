#Read and process Medlyn et al 2002 (Pinus pinaster data)

  path_to_data<-paste0(path,"/data_with_gs/Pinus")
  setwd(path_to_data)
  
  files.summer <- list.files(pattern="\\.xls")
  allxlfiles.s<-list()
  
  for (i in seq_along(files.summer)){
    
 
    dat <- suppressWarnings(read_excel(files.summer[i],sheet="Data"))
    dat <- dat[-c(1:3),]
    dat <- dat[,c(1:32)]
    dat$spp<- files.summer[i]
    
  
    #dat <- subset(dat,!is.na(as.numeric(dat$Obs)))
 
    allxlfiles.s[[i]] <- dat
    
  }
  
  summer<-data.frame(do.call(rbind.fill,allxlfiles.s))
  summer<-subset(summer,!is.na(summer$pousse))

  
  #to remove space between species names and season names
  
  summer$spp<-factor(str_replace_all(summer$spp, fixed(" "), "") )

  summer$Species<-str_sub(summer$spp, start = 1, end = 3)
  summer$Month<-str_sub(summer$spp, start = 4, end = 5)

  med_gs_dat<-summer[,c("Species","Month","CO2abs","Tleaf","E","A","gH2O","gCO2","Ci","ALVPD")]



