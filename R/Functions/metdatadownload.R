#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------

#-- Script download met data from WTC and eMAST database
#-- 
#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------
#install.packages("dismo")
#install.packages('chron')
#install.packages('sp')
install_bitbucket("remkoduursma/HIEv")
#install.packages("rgbif")

library(rgbif)  # when using GBIF
library(ALA4R)  # when using ALA
library(raster)
library(oz)
library(chron)
library(sp)
library(devtools)
library(HIEv)
library(dismo)

setToken(tokenfile="mytocken.txt")


if(!dir.exists("MET_DATA"))dir.create("MET_DATA")
if(!dir.exists("MET_DATA/WTC1"))dir.create("MET_DATA/WTC1")
if(!dir.exists("MET_DATA/WTC2"))dir.create("MET_DATA/WTC2")
if(!dir.exists("MET_DATA/WTC3"))dir.create("MET_DATA/WTC3")
if(!dir.exists("MET_DATA/WTC4"))dir.create("MET_DATA/WTC4")
if(!dir.exists("MET_DATA/HFE"))dir.create("MET_DATA/HFE")
if(!dir.exists("MET_DATA/eMAST"))dir.create("MET_DATA/eMAST")

#Download MET Files from the WTC experiment (WTC 3 and WTC 4)
metWTC4<-metD(path="C:/Dushan/Repos/PhotoM/MET_DATA/WTC4",pattern="PARRA_CM_WTCMET")
metWTC3<-metD(path="C:/Dushan/Repos/PhotoM/MET_DATA/WTC3",pattern="TEMP_CM_WTCMET")
metWTC1<-metD(path="C:/Dushan/Repos/PhotoM/MET_DATA/WTC1",pattern="CO2DROUGHT_CM_WTCFLUX_20080220-20090316")


#first search hieV for file name "WTCMET"
#metWTC4<-searchHIEv("PARRA_CM_WTCMET",quiet=TRUE)

#setToPath("MET_DATA/WTC4")   #set path to the folder which save the downloaded files

#to download data
#downloadHIEv(metWTC4)

#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------
#download Tmin and Tmax fro species climate of origine
#this get data from eMAST database
#set path to the directory where downloaded files to be saved

path<-"C:/Users/90931217/Documents/Dushan/Repos/PhotoM/MET_DATA/eMAST"


getmet(path=path,var1="tmin",var2="tmax",lat=-38.8,lon=143.59,site="wtc2")
getmet(path=path,var1="tmin",var2="tmax",lat=-33.62,lon=150.74,site="wtc3")
getmet(path=path,var1="tmin",var2="tmax",lat=-30.5675,lon=152.1461,site="S30ESa")
getmet(path=path,var1="tmin",var2="tmax",lat=-32.9852 ,lon=147.8852,site="S30ESy")
getmet(path=path,var1="tmin",var2="tmax",lat=-35.6557,lon=148.1520,site="tmb")
getmet(path=path,var1="tmin",var2="tmax",lat=-14.1591,lon=131.3880,site="daly")
getmet(path=path,var1="tmin",var2="tmax",lat=-30.1913,lon=120.6541,site="gww")
getmet(path=path,var1="tmin",var2="tmax",lat=-33.03,lon=138.75,site="hfecla")
getmet(path=path,var1="tmin",var2="tmax",lat=-35.11 ,lon=147.37,site="hfemel")
getmet(path=path,var1="tmin",var2="tmax",lat=-33.61 ,lon=150.81,site="hfecre")
getmet(path=path,var1="tmin",var2="tmax",lat=-30.04 ,lon=150.73,site="hfeter")
getmet(path=path,var1="tmin",var2="tmax",lat=-28.41 ,lon=153.02,site="hfedun")
getmet(path=path,var1="tmin",var2="tmax",lat=-30.37  ,lon=152.10,site="hfesal")
getmet(path=path,var1="tmin",var2="tmax",lat=-16.11  ,lon=145.45,site="rf")
getmet(path=path,var1="tmin",var2="tmax",lat=-30.4335  ,lon=152.0421,site="wtc1")
getmet(path=path,var1="tmin",var2="tmax",lat=-33.62  ,lon=150.74,site="local")
getmet(path=path,var1="tmin",var2="tmax",lat=-42.81  ,lon=146.61,site="tasmania")
#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------

#Download worldclim raster data
topath="C:/Users/90931217/Documents/Dushan/WorldClim"
get_worldclim_rasters_new(topath=topath)

#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------

##Download temperature data for datasets out Australia

path="C:/Users/90931217/Documents/Dushan/WorldClim"

sites<-read.csv("locations_oaz.csv")

loc<-split(sites,sites$site)

for(i in 1:length(loc)){
  dat<-(loc[i])
  getmetworld(path=path,lat=dat[[1]][[2]],lon=dat[[1]][[3]],site=dat[[1]][[1]])
}

#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------


#path="C:/Users/90931217/Documents/Dushan/WorldClim"
#getmetworld(path=path,lat=42,lon=89,site="USA1")


#data<-read.table("CRA.USA.txt",header = FALSE,skip=15,sep="")
#wtcMet2(path=path,from="2011-07-15",fname="Tropical_anjelica.csv") 
#Function to download DUKE FACE met data (for Ellsworth et al data)
#-----------------------------------------------------------------------------------------
#-----------------------------------------------------------------------------------------

getmetduke<-function(path){
  
  o <- getwd()
  on.exit(setwd(o))
  setwd(path)
  
  
  base <-"ftp://cdiac.ornl.gov/pub/FACE/FACE-MDS/Met_Data/Phase1/DUKE_forcing_d.txt"
  
  desf.1<-paste0(path,"/Duke_met_data.txt")
  download.file(url=base, destfile=desf.1,cacheOK = TRUE)
  
  data<-read.table("Duke_met_data.txt",header=TRUE,sep="",skip=1)
  data<-data[,c(1,2,4)]
  names(data)<-c("Year","DOY","Temp")
  data$Tmean<-data$Temp-273.15
  
  data$DateTime <-seq(from=as.Date("1996-01-01"),to=as.Date("2007-12-31"),by="day")
  write.csv(data,paste0(path,"/duke_temp_data.csv",collapse = " "))
  
  
  #write.csv(fl2,paste0(path,sprintf("/%s.%s.csv",site,var2),collapse = " "))
  
}



#---------------------------------------------------------------------------------
#---------------------------------------------------------------------------------
#this script download species occurence location and mean temperature for each location
#different temperature indices (MAT,MAmaxT,MAminT and theit 5th and 95th percentiles)
#---------------------------------------------------------------------------------
#---------------------------------------------------------------------------------

#install.packages("rgbif")
#install.packages("ALA4R")
#install.packages("raster")
#install.packages("rgdal")

library(rgbif)  # when using GBIF
library(ALA4R)  # when using ALA
library(raster)
library(oz)

# Downloads the script into your working directory
url <- "https://bitbucket.org/!api/2.0/snippets/remkoduursma/pp6R4/263ee377b87bf487b3687c90755f7ef658f71151/files/ala_gbif_worldclim_species_envelope.r"
download.file(url, basename(url))

# and source
source("ala_gbif_worldclim_species_envelope.r")
#---------------------------------------------------------------------------------
#---------------------------------------------------------------------------------

#set topath to a directory where the downloaded WORLDCLIM rasters to be saved
#currently, this download WORLDCLIM met data with 30s resolution 
topath<-"C:/Users/90931217/Documents/Dushan/WorldClim"
#---------------------------------------------------------------------------------
#---------------------------------------------------------------------------------

#restart R then  install.packages(c("curl", "httr"))  then try again.
#download met data for Australian species

species_list_eucs<-read.csv("species_list_eucs.csv")
species_list_eucs$Species<-as.character(species_list_eucs$Species)
met.all_eucs<-list()
for(i in 1:length(species_list_eucs$Species)){
  spp<-species_list_eucs$Species[[i]]
  occ<-get_occurrences_ala(spp)
  met<-get_worldclim_temp(occ,topath=topath,return="summary")
  
  met.all_eucs[[i]] <- met
  metdat_eucs<-do.call(rbind.data.frame,met.all_eucs)
}

#occ<-get_occurrences_ala("Eucalyptus microcorys")
#met<-get_worldclim_temp(occ,topath=topath,return="summary")


#metdat_eucs_new<-rbind(metdat_eucs,met)
#metdat_eucs_new<-rbind(metdat_eucs_new,met)
save(metdat_eucs,file="metdat_eucs,RData")
load("metdat_eucs,RData")

#---------------------------------------------------------------------------------
#---------------------------------------------------------------------------------

#download met data for other species

#---------------------------------------------------------------------------------
#---------------------------------------------------------------------------------

species_list_others<-read.csv("species_list_others.csv")
species_list_others$Species<-as.character(species_list_others$Species)
met.all_others<-list()
for(i in 1:length(species_list_others$Species)){
  spp<-species_list_others$Species[[i]]
  occ<-get_occurrences_gbif(spp)
  met<-get_worldclim_temp(occ,topath=topath,return="summary")
  met.all_others[[i]] <- met
  metdat_others<-do.call(rbind.data.frame,met.all_others)
}

save(metdat_others,file="met_others,RData")
load("met_others,RData")

#---------------------------------------------------------------------------------
#---------------------------------------------------------------------------------
#get E.tereticornis
#this was missed from the preveoue euc list
#topath<-"//ad.uws.edu.au/dfshare/HomesHWK$/90925395/My Documents/Repos/PhotoM/WorldClim"
#occ<-get_occurrences_ala("Eucalyptus maidenii")
#spp_eteretic<-get_worldclim_temp(occ,topath=topath,return="summary")
#names(spp_eteretic)[1]<-"species_new"


spp_clim_env<-rbind(metdat_eucs,metdat_others)
#spp_clim_env<-rbind(spp_clim_env,spp_eteretic)
save(spp_clim_env,file="spp_clim_env,RData")
load("spp_clim_env,RData")
write.csv(spp_clim_env,paste0(path,"/Tables/spp_clim_env.csv"),row.names=FALSE,sep=",")
#---------------------------------------------------------------------------------
#---------------------------------------------------------------------------------

#download met data for species' seed source

#---------------------------------------------------------------------------------
#---------------------------------------------------------------------------------
#topath<-"//ad.uws.edu.au/dfshare/HomesHWK$/90931217/My Documents/Documents/Dushan/WorldClim"

df<-read.csv(paste0(path,"/dataset_seedsource_info.csv"))
metdat_ss<-get_worldclim_temp_seedsource(topath=topath,data=df,return="summary")

metdat_ss<-merge(metdat_ss,df,by=c("species"))
metdat_ss<-metdat_ss[-c(1,7,8)]
names(metdat_ss)[1:3]<-c("MATSS","MAXSS","MINSS")

write.csv(metdat_ss,paste0(path,"/Tables/metdat_ss.csv"),row.names=FALSE,sep=",")

#save(metdat_ss,file="met_seed_sources,RData")
#load("met_seed_sources,RData")


#dfr<-read.csv(paste0(topath,"/locations_euc_maidenii.csv"))
#get_worldclim_temp_seedsource(topath=topath,data=dfr,return="summary")

#occ<-get_occurrences_ala("Eucalyptus crebra")
#---------------------------------------------------------------------------------
#---------------------------------------------------------------------------------

get_worldclim_rasters_new<-function(topath, clean=FALSE){
  
  download_worldclim <- function(basen, topath){
    
    wc_fn_full <- file.path(topath, basen)
    
    if(!file.exists(wc_fn_full)){
      message("Downloading WorldClim 10min layers ... ", appendLF=FALSE)
      download.file(file.path("http://biogeo.ucdavis.edu/data/climate/worldclim/1_4/grid/cur",basen),
                    wc_fn_full, mode="wb")
      message("done.")
    }
    
    u <- unzip(wc_fn_full, exdir=topath)
    
    return(u)
  }
  
  download_worldclim("tmean_30s_esri.zip", topath)
  download_worldclim("tmax_30s_esri.zip", topath) 
  download_worldclim("tmin_30s_esri.zip", topath) 
  
  if(clean){
    unlink(c(wc_fn_full,dir(file.path(topath,"tmean"),recursive=TRUE)))
    unlink(c(wc_fn_full,dir(file.path(topath,"tmax"),recursive=TRUE)))
    unlink(c(wc_fn_full,dir(file.path(topath,"tmin"),recursive=TRUE)))
  }
  
  # Read the rasters into a list
  tmean_raster <- list()
  tmax_raster <- list()
  tmin_raster <- list()
  
  #message("Reading Worldclim rasters ... ", appendLF = FALSE)
  for(i in 1:12){
    tmean_raster[[i]] <- raster(file.path(topath, sprintf("tmean/tmean_%s", i)))
    tmax_raster[[i]] <- raster(file.path(topath, sprintf("tmax/tmax_%s", i)))
    tmin_raster[[i]] <- raster(file.path(topath, sprintf("tmin/tmin_%s", i)))
    
  }
  #message("done.")
  
  return(list(tmean_raster=tmean_raster, tmax_raster=tmax_raster,tmin_raster=tmin_raster))
}

#get_worldclim_rasters_new(topath=topath)
