#---------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------



# Plot parameters of temperature response of Vcmax and Jmax (seasonal datasets)
# Manuscript Figure 4 


#---------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------

  
  path<-getwd()
  
  #########################################################################################################################################
  #Read biochemical data
  
  tfits_VJ<-read.csv(paste0(path,"/Tables/biochemical_parameters_with_met_data_with_rd.csv"))
  #tfits_VJ<-tfits_VJ[c("Species", "DataSet","Season_New","Temp_Treatment","Vcmax25","Topt","b","Aopt.se","Topt.se","Tavg_30",          
  #"b.se","PFT","BIO1","BIO5","BIO7")]
  
  
  tfits_VJ$TPUFrac<-with(tfits_VJ,nTPU/nVcmax)
  tfits_VJ$MAXSS<-with(tfits_VJ,(BIO5/10))
  tfits_VJ$Tdiff_m<-with(tfits_VJ,Tavg_30-(BIO5/10))
  tfits_VJ$Tdiff<-with(tfits_VJ,Tavg_30-gMDD0)
  
  #Get seasonal data seperately
  
  seasons_vj<-subset(tfits_VJ,tfits_VJ$Season_New %in% c("Summer","Autumn","Spring","Winter","Dry","Wet"))
  seasons_vj<-subset(seasons_vj,seasons_vj$Temp_Treatment %in% c("ambient","Ambient","No_Temperature_Treatments")) # exclude elevated temperature treatments
  
  seasons_vj.1<-subset(seasons_vj, ! DataSet %in% c("RF_AUS","SAVANNA","OND")) #not seasonal data
  seasons_vj.1$DataSet<-factor(seasons_vj.1$DataSet)
  
  #
  
  seasons_vj.2<-seasons_vj.1[c("Species","DataSet","Season_New","Tavg_30","Topt_275","TPUFrac","gMDD0","Tdiff","Tdiff_m")]
  seasons_vj.2<-subset(seasons_vj.2,!is.na(Topt_275)&seasons_vj.2$DataSet!="TMB")
  seasons_vj.2$DataSet<-factor(seasons_vj.2$DataSet)
  
  ###########################################################################################################################################
  #############################################################################################################
  
   #------------------------------------------------
  
  COL<-palette(c("#B15928","#FFFF99","#FDBF6F","#CAB2D6","darkorange1","#6A3D9A","#E31A1C","#FB9A99","#33A02C","#B2DF8A","#1F78B4","#A6CEE3","#F781BF", "#A65628","#4DAF4A"))
  COL<-palette(c("#B15928","#FFFF99","#FDBF6F","#CAB2D6","darkorange1","#6A3D9A","#E31A1C","#FB9A99","#33A02C","#B2DF8A","#1F78B4","#A6CEE3","#F781BF", "#A65628","#4DAF4A"))
  
  COL.1<-COL[c(1:7,10:13)]
  #Vcmax and Jmax at 25C
  # windows(120,180);
  
  # png(filename="manuscript_figures/figure_4.png",width = 500, height = 500*1.6)
  # par(cex.lab=1.5,mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,5),cex.axis=1.5,las=1,mfrow=c(4,2))
  
  pdf("manuscript_figures/figure_3.pdf",width = 5, height = 6)
  par(cex.lab=1.5,mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,5),cex.axis=1.5,las=1,mfrow=c(3,2))
  
 
  #------------------------------------
  #------------------------------------
  
  #- Topt for Vcmax
  
  with(seasons_vj.1,plot(Tavg_30,Topt_vcmax_PA,bg=COL.1[seasons_vj.1$DataSet],pch=21,cex=.2,ylab="",xlab="",axes=F,xlim=c(0,30),ylim=c(20,60)))
  magaxis(side=c(1,2,4),labels=c(0,1,0),frame.plot=T,majorn = 4)
  
  title(ylab=expression(Topt[V]~(degree*C)),
        xpd=NA,cex.lab=1.6)
  
  ebd.1<-subset(seasons_vj.1,seasons_vj.1$Topt_vcmax_SE_PA<10)
  adderrorbars(x=seasons_vj.1$Tavg_30,y=seasons_vj.1$Topt_vcmax_PA,SE=ebd.1$Topt_vcmax_SE_PA ,direction="updown",col=COL.1[seasons_vj.1$DataSet])
  
  legend("topright",expression((bold(a))),bty="n",cex=1.5)
  
  lm_topt<-lmer(Topt_vcmax_PA~Tavg_30+(1|DataSet),data=seasons_vj.1,weights=1/seasons_vj.1$Topt_vcmax_SE_PA)
  summary(lm_topt)$coefficients
  
    
  # ablineclip(33.7959, 0.1839, x1 = min(seasons_vj.1$Tavg_30,na.rm=T)-5, x2 = max(seasons_vj.1$Tavg_30,na.rm=T)+5,col="black",lwd=2,lty=1)
  
  with(seasons_vj.1,points(Tavg_30,Topt_vcmax_PA,bg=COL.1[seasons_vj.1$DataSet],col=COL.1[seasons_vj.1$DataSet],pch=21,cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,30),ylim=c(20,60)))
  
  # add regression line to the plot
  ablineclip(a= summary(lm_topt)$coefficients[1],b=summary(lm_topt)$coefficients[2],x1=min(t_home_bio$Tavg_30)-2,x2=max(t_home_bio$Tavg_30)+2,
             y1=min(t_home_bio$Topt_vcmax_PA)-2,y2=max(t_home_bio$Topt_vcmax_PA)+2,lty=1,col="black",lwd=2)
  
  
  # #--------------------------
  # #- add Kattge & Knorr model to the plot
  ablineclip(a=24.92,b=0.44,x1=min(seasons_vj.1$Tavg_30,na.rm=T)-3,x2=max(seasons_vj.1$Tavg_30,na.rm=T)+3,lty = 3,lwd=2)
  # 
  # #--------------------------
  
  
  
  #- Topt for Jmax
  with(seasons_vj.1,plot(Tavg_30,Topt_jmax_PA,bg=COL.1[seasons_vj.1$DataSet],pch=21,cex=.2,ylab="",xlab="",axes=F,xlim=c(0,30),ylim=c(20,60)))
  magaxis(side=c(1,2,4),labels=c(0,0,1),frame.plot=T,majorn = 4)
  
  ebd.1<-subset(seasons_vj.1,seasons_vj.1$Topt_Jmax_SE_PA<10)
  adderrorbars(x=seasons_vj.1$Tavg_30,y=seasons_vj.1$Topt_jmax,SE=ebd.1$Topt_Jmax_SE_PA ,direction="updown",col=COL.1[seasons_vj.1$DataSet])
  
  
  title(ylab=expression(Topt[J]~(degree*C)),
        xpd=NA,cex.lab=1.6,line=-18)
  
  legend("topright",expression((bold(d))),bty="n",cex=1.5)
  
  
  # ablineclip(28.63652, 0.31372, x1 = min(seasons_vj.1$Tavg_30,na.rm=T)-5, x2 = max(seasons_vj.1$Tavg_30,na.rm=T)+5,col="black",lwd=2,lty=1)
  
  lm_toptj<-lmer(Topt_jmax_PA~Tavg_30+(1|DataSet),data=seasons_vj.1,weights=1/seasons_vj.1$Topt_Jmax_SE_PA)
  summary(lm_toptj)$coefficients
  
  
  
  
  with(seasons_vj.1,points(Tavg_30,Topt_jmax_PA,bg=COL.1[seasons_vj.1$DataSet],pch=21,cex=1.5,ylab="",col=COL.1[seasons_vj.1$DataSet],
                           xlab="",axes=F,xlim=c(0,30),ylim=c(20,50)))
  
  
  # add regression line to the plot
  ablineclip(a= summary(lm_toptj)$coefficients[1],b=summary(lm_toptj)$coefficients[2],x1=min(t_home_bio$Tavg_30)-2,x2=max(t_home_bio$Tavg_30)+2,
             y1=min(t_home_bio$Topt_jmax_PA,na.rm=T)-2,y2=max(t_home_bio$Topt_jmax_PA,na.rm=T)+2,lty=1,col="black",lwd=2)
  
  
  # #--------------------------
  # #- add Kattge & Knorr model to the plot
  ablineclip(a=26.21,b=0.33,x1=min(seasons_vj.1$Tavg_30,na.rm=T)-3,x2=max(seasons_vj.2$Tavg_30,na.rm=T)+3,lty = 3,lwd=2)
  # 
  # #--------------------------
  # 
  
  #------------------------------------
  #------------------------------------
  
  # Ea for Vcmax and Jmax
  
  #- Ea Vcmax
  with(seasons_vj.1,plot(Tavg_30,EaV,bg=COL.1[seasons_vj.1$DataSet],pch=21,cex=.2,ylab="",xlab="",axes=F,xlim=c(0,30),ylim=c(0,150)))
  magaxis(side=c(1,2,4),labels=c(0,1,0),frame.plot=T,majorn = 4)
  
  aeb<-subset(seasons_vj.1,seasons_vj.1$EaV<100)
  adderrorbars(x=seasons_vj.1$Tavg_30,y=seasons_vj.1$EaV,SE=aeb$EaV.se ,direction="updown",col=COL.1[seasons_vj.1$DataSet])
  title(ylab=expression(Ea[v]~(kJ~mol^-1)),
        xpd=NA,cex.lab=1.6)
  legend("topright",expression((bold(b))),bty="n",cex=1.5)
  
  # ablineclip(42.6436, 1.1427, x1 = min(seasons_vj.1$Tavg_30,na.rm=T)-5, x2 = max(seasons_vj.1$Tavg_30,na.rm=T)+5,col="black",lwd=2,lty=1)
  
  # remove very high EaV before fitting regression (one data point of >150 kJmol-1)
  seasons_vj.x<-subset(seasons_vj.1,seasons_vj.1$EaV<150)
  lm_ev<-lmer(EaV~Tavg_30+(1|DataSet),data=seasons_vj.x,weights=1/seasons_vj.x$EaV.se)
  Anova(lm_ev)
  rsquared.glmm(lm_ev)
  
  with(seasons_vj.1,points(Tavg_30,EaV,bg=COL.1[seasons_vj.1$DataSet],pch=21,col=COL.1[seasons_vj.1$DataSet],
                           cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,30),ylim=c(0,150)))
 
  # add regression line to the plot
  ablineclip(a= summary(lm_ev)$coefficients[1],b=summary(lm_ev)$coefficients[2],x1=min(seasons_vj.1$Tavg_30)-2,x2=max(seasons_vj.1$Tavg_30)+2,
             y1=min(seasons_vj.1$EaV,na.rm=T)-2,y2=100,lty=1,col="black",lwd=2)
  
  
  
  
   
  #------------------------------------
  #------------------------------------
  
  #- Ea Jmax
  with(subset(seasons_vj.1,seasons_vj.1$EaJ<90),plot(Tavg_30,EaJ,bg=COL.1[seasons_vj.1$DataSet],pch=21,cex=.2,ylab="",xlab="",axes=F,xlim=c(0,30),ylim=c(0,150)))
  magaxis(side=c(1,2,4),labels=c(0,0,1),frame.plot=T,majorn = 4)
 
   adderrorbars(x=subset(seasons_vj.1,seasons_vj.1$EaJ<90)$Tavg_30,y=subset(seasons_vj.1,seasons_vj.1$EaJ<90)$EaJ,
                SE=subset(seasons_vj.1,seasons_vj.1$EaJ<90)$EaJ.se ,direction="updown",col=COL.1[seasons_vj.1$DataSet])
  
  title(ylab=expression(Ea[J]~(kJ~mol^-1)),
        xpd=NA,cex.lab=1.6,line=-18)
  legend("topright",expression((bold(e))),bty="n",cex=1.5)
  
  with(subset(seasons_vj.1,seasons_vj.1$EaJ<90),points(Tavg_30,EaJ,bg=COL.1[seasons_vj.1$DataSet],pch=21,cex=1.5,ylab="",col=COL.1[seasons_vj.1$DataSet],
                                                       xlab="",axes=F,xlim=c(0,30),ylim=c(0,150)))
  
  lm_ej<-lmer(EaJ~Tavg_30+(1|DataSet),data=seasons_vj.1,weights=1/seasons_vj.1$EaJ.se)
  summary(lm_ej)$coefficients
  
  #------------------------------------
  #------------------------------------
  
  #- deltaS vcmax
  
  with(seasons_vj.1,plot(Tavg_30,delsV*1000,bg=COL.1[seasons_vj.1$DataSet],pch=21,cex=.2,ylab="",xlab="",axes=F,xlim=c(0,30),ylim=c(600,680)))
  magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T,majorn = 4)
  title(ylab=expression(Delta*S[V]~(J~mol^-1~K^-1)),
        xpd=NA,cex.lab=1.6)
  
  ebd.1<-subset(seasons_vj.1,seasons_vj.1$delsV.se<0.01)
  adderrorbars(x=ebd.1$Tavg_30,y=ebd.1$delsV*1000,SE=ebd.1$delsV.se*1000 ,direction="updown",col=COL.1[seasons_vj.1$DataSet])
  legend("topright",expression((bold(c))),bty="n",cex=1.5)
  
  
  #--------------------------
  #- add Kattge & Knorr model to the plot
  ablineclip(a=668.39,b=-1.07,x1=min(seasons_vj.1$Tavg_30,na.rm=T)-3,x2=max(seasons_vj.1$Tavg_30,na.rm=T)+3,lty = 3,lwd=2)
  
  #--------------------------
  seasons_vj.1$DSV<-with(seasons_vj.1,delsV*1000)
  seasons_vj.1$DSV.se<-with(seasons_vj.1,delsV.se*1000)
  
  lm_dv<-lmer(DSV~Tavg_30+(1|DataSet),data=seasons_vj.1,weights=1/seasons_vj.1$DSV.se)
  Anova(lm_dv)
  rsquared.glmm(lm_dv)
  
   ablineclip(a= summary(lm_dv)$coefficients[1],b=summary(lm_dv)$coefficients[2],x1=min(seasons_vj.1$Tavg_30)-2,x2=max(seasons_vj.1$Tavg_30)+2,
              y1=min(seasons_vj.1$DSV,na.rm=T)-2,y2=max(seasons_vj.1$DSV,na.rm=T)+2,lty=1,col="black",lwd=2)
  # 
  # 
   with(seasons_vj.1,points(Tavg_30,delsV*1000,bg=COL.1[seasons_vj.1$DataSet],pch=21,cex=1.5,ylab="",xlab="",col=COL.1[seasons_vj.1$DataSet],axes=F,xlim=c(0,30),ylim=c(600,680)))
   
   
   
  # lm_dv<-lmer(DSV~Tavg_30+(1|DataSet),data=subset(seasons_vj.1,seasons_vj.1$DataSet!="MEDLYN"),weights=1/subset(seasons_vj.1,seasons_vj.1$DataSet!="MEDLYN")$DSV.se)
  # Anova(lm_dv)
  # rsquared.glmm(lm_dv)
  # 
  #------------------------------------
  #------------------------------------
  
  #- deltaS Jmax
  seasons_vj.1$DSJ<-with(seasons_vj.1,delsJ*1000)
  seasons_vj.1$DSJ.se<-with(seasons_vj.1,delsJ.se*1000)
  
  with(seasons_vj.1,plot(Tavg_30,delsJ*1000,bg=COL.1[seasons_vj.1$DataSet],pch=21,cex=.2,ylab="",xlab="",axes=F,xlim=c(0,30),ylim=c(600,680)))
  magaxis(side=c(1,2,4),labels=c(1,0,1),frame.plot=T,majorn = 4)
  
  ebd.1<-subset(seasons_vj.1,seasons_vj.1$delsJ.se<0.03)
  adderrorbars(x=ebd.1$Tavg_30,y=ebd.1$delsJ*1000,SE=ebd.1$delsJ.se*1000 ,direction="updown",col=COL.1[seasons_vj.1$DataSet])
  legend("topright",expression((bold(f))),bty="n",cex=1.5)
  
  
  title(ylab=expression(Delta*S[J]~(J~mol^-1~K^-1)),
        xpd=NA,cex.lab=1.6,line=-18)
  
  
  # ablineclip(0.6491615*1000, -0.0006312*1000, x1 = min(seasons_vj.1$Tavg_30,na.rm=T)-5, x2 = max(seasons_vj.1$Tavg_30,na.rm=T)+5,col="black",lwd=2,lty=1)
  
  lm_dj<-lmer(DSJ~Tavg_30+(1|DataSet),data=seasons_vj.1,weights=1/seasons_vj.1$DSJ.se)
  Anova(lm_dj,test="F")
  rsquared.glmm(lm_dj)
  
  with(seasons_vj.1,points(Tavg_30,delsJ*1000,bg=COL.1[seasons_vj.1$DataSet],pch=21,cex=1.5,ylab="",xlab="",col=COL.1[seasons_vj.1$DataSet],axes=F,xlim=c(0,30),ylim=c(600,680)))
  
  
  # add regression line to the plot
  ablineclip(a= summary(lm_dj)$coefficients[1],b=summary(lm_dj)$coefficients[2],x1=min(seasons_vj.1$Tavg_30)-2,x2=max(seasons_vj.1$Tavg_30)+2,
             y1=min(seasons_vj.1$DSJ,na.rm=T)-2,y2=max(seasons_vj.1$DSJ,na.rm=T)+2,lty=1,col="black",lwd=2)
  
  
  
  
   
  # #--------------------------
  # #- add Kattge & Knorr model to the plot
  ablineclip(a=659.70,b=-0.75,x1=min(seasons_vj.1$Tavg_30,na.rm=T)-3,x2=max(seasons_vj.1$Tavg_30,na.rm=T)+3,lty = 3,lwd=2)
  # 
  # #--------------------------
  
  
  
  title(xlab=expression(~T[growth]~(degree*C)),cex.lab=1.6,outer=T,adj=0.2,line=2)
  title(xlab=expression(~T[growth]~(degree*C)),cex.lab=1.6,outer=T,adj=0.875,line=2)
  
  
  # title(xlab=expression(Growth~T[air]~(degree*C)),cex.lab=1.6,outer=T,adj=0.5,line=3)
  
  # title(xlab=expression(Growth~T[air]~(degree*C)),cex.lab=1.5,outer=T,adj=0.2,line=3)
  # title(xlab=expression(Growth~T[air]~(degree*C)),cex.lab=1.5,outer=T,adj=0.9,line=3)
  dev.off()
  #--------------------------------------------------------------------------------------------------------------------------
  #--------------------------------------------------------------------------------------------------------------------------
