
lmm_topt<-lmer(Topt~Tdiff+(1|Species),data=subset(seasons.2,seasons.2$DataSet != "HAN_ET_AL_A")) #random intercept model. Dataset as random factor
lmm_topt.1<-lmer(Topt~Tdiff+(Tdiff|Species),data=subset(seasons.2,seasons.2$DataSet != "HAN_ET_AL_A")) #model with random slope and intercept  

Anova(lmm_topt,test="F")
Anova(lmm_topt.1,test="F")

anova(lmm_topt,lmm_topt.1) 
rsquared.glmm(lmm_topt)



#add best fitted linear mix model)
lmm_275<-lmer(Topt_275~Tdiff+(1|Species),data=subset(seasons_vj.2,seasons_vj.2$Topt_275<38)) #random intercept model. Dataset as random factor
lmm_275.1<-lmer(Topt_275~Tdiff+(Tdiff|Species),data=subset(seasons_vj.2,seasons_vj.2$Topt_275<38)) #model with random slope and intercept 

Anova(lmm_275,test="F")
Anova(lmm_275.1,test="F")

anova(lmm_275,lmm_275.1) 
rsquared.glmm(lmm_275.1)#random slope model is marginally significant

#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------

#---- JV ratio acclimation model

lm6<-lmer(JVr~Tdiff_m+(Tdiff_m|DataSet),data=seasons_vj.3) 
Anova(lm6,test="F")
qqPlot(residuals(lm6))
plot(resid(lm6) ~ fitted(lm6))
abline(h=0)
# 
# summary(lm6)
rsquared.glmm(lm6)


jvr<-fit_lmm(seasons_vj.3,yvar="JVr",xvar="Tdiff_m",rand="DataSet",method="randIS")


#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------

#---------------
# Acclimation model for delS(Vcmax)

lmm_delsv<-lmer(delsV~Tdiff_m+(1|Species),data=seasons_vj.3) #random intercept model. Dataset as random factor
lmm_delsv.1<-lmer(delsV~Tdiff_m+(Tdiff_m|Species),data=seasons_vj.3) #model with random slope and intercept 

Anova(lmm_delsv,test="F")
Anova(lmm_delsv.1,test="F")

#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------

#---------------

# Acclimation model for delS(Jmax)
lmm_delsj<-lmer(delsJ~Tdiff_m+(1|Species),data=seasons_vj.3) #random intercept model. Dataset as random factor
lmm_delsj.1<-lmer(delsJ~Tdiff_m+(Tdiff_m|Species),data=seasons_vj.3) #model with random slope and intercept 

Anova(lmm_delsj,test="F")
Anova(lmm_delsj.1,test="F")
anova(lmm_delsj,lmm_delsj.1) 

rsquared.glmm(lmm_delsj)

qqPlot(residuals(lmm_delsj))
plot(resid(lmm_delsj) ~ fitted(lmm_delsj))
abline(h=0)

#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------

#---------------

lmm_eav<-lmer(EaV~Tdiff_m+(1|DataSet),data=subset(seasons_vj.3,seasons_vj.3$EaV<100)) #random intercept model. Dataset as random factor
lmm_eav.1<-lmer(EaV~Tdiff_m+(Tdiff_m|Species),data=subset(seasons_vj.3,seasons_vj.3$EaV<100)) #model with random slope and intercept 

Anova(lmm_eav,test="F")
Anova(lmm_eav.1,test="F")
anova(lmm_eav,lmm_eav.1) 

rsquared.glmm(lmm_eav)

#--------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------------------------------
#- use temperature acclimation and adaptation functions to derive Topt for photosynthesis at a common Ci
#- model used: Photosyn (plantecophys package); coupled photosynthesis-stomatal conductance model

# read biochemical parameters

tfits_VJ<-read.csv(paste0(path,"/Tables/biochemical_parameters_with_met_data.csv"))

# calculate maximum temperature of the warmest quarter
tfits_VJ$MAXSS<-with(tfits_VJ,BIO5/10)

# for tropical species, Tgrowth is not available in some cases. So set to long-term mean temperature as there is no significant
# seasonal effects

tfits_VJ$Tavg_30<-ifelse(is.na(tfits_VJ$Tavg_30),tfits_VJ$gMDD0,tfits_VJ$Tavg_30)


# get the difference between long-term average growing season temperature and growth temperature
# acclimation to growth temperature is based on the difference


tfits_VJ$Tdiff<-with(tfits_VJ,Tavg_30-gMDD0)

tfits_VJ$Tdiff_m<-with(tfits_VJ,Tavg_30-MAXSS)



# function to model photosynthesis

model_photo_newf<-function(data){
  
  # get a sequence of leaf temperatures
  Temps <- seq(15,40,by=0.1)
  Tdew=15
  VPD <- DewtoVPD(Tdew=Tdew,TdegC=Temps)
  
  
  # Vcmax - PFT specific
  Vcmax25<-data$Vcmax25
  
  
  # Activation energy for Jmax, PFT specific
  
  EaJ<-data$EaJ*1000
  
  # deltaS for Vcmax, PFT specific
  delsV<-data$delsV*1000
  #delsV<-with(data,(661.24-(1.24*gMDD0)))
  
  
  #dS for Jmax
  #both adaptationa and acclimation included. when Tgrowth > Thome, dS decrease and vise versa
  
  delsJ<-with(data,662.7-(0.97*MAXSS)+(-.5555*Tdiff_m)) # adaptation from commongarden experiments
  
  # Jmax:Vcmax at 25C
  #both adaptationa and acclimation included. when Tgrowth > Thome, JVr decrease and vise versa
  jvr<-with(data,2.4-(0.021607*MAXSS)+(-0.03153*Tdiff_m)) # adaptation from commongarden experiments
  
  # get Jmax as a function of Vcmax
  Jmax25<-Vcmax25*jvr
  
  # Activation energy for Vcmax
  # accimated for growth temperature
  
  
  EaV<-with(data,42.6+(1.14*Tavg_30))*1000  #acclimation only
  
  #EaV<-with(data,42.6+(1.14*Tavg_30))*1000  #acclimation only
  
  # model photosynthesis at a common Ci=275 ppm
  out <- Photosyn(Tleaf=Temps,VPD=VPD,Ci=275,
                  Vcmax=Vcmax25,EaV=EaV,EdVC=2e5,delsC=delsV,
                  Jmax = Jmax25,EaJ=EaJ,EdVJ=2e5,delsJ=delsJ)
  
  return(out)
}


# model photosynthesis

vcmax.1<-t_home_bio[c("Species", "DataSet","Season_New","Temp_Treatment","Tavg_30","Topt_275","Topt_275.se","gMDD0","Rday","Tdiff","PFT","Tdiff_m","MAXSS")]
vcmax.1$PFT[vcmax.1$DataSet=="SAVANNA"] <- "TET_TE" #change PFT


# add PFT specific parameters to the dataframe
vcmax.1<-merge(vcmax.1,pft_param,by="PFT",all=T)
vcmax.1$DataSet<-factor(vcmax.1$DataSet)
vcmax.1$Species<-factor(vcmax.1$Species)



dat_mod_1<-split(vcmax.1,factor(vcmax.1$DataSet))


mod_photo_3<-lapply(dat_mod_1,FUN=model_photo_newf)
topt_mod_3<-get_topts(lapply(mod_photo_3,FUN=topt_from_photosyn))

topt_mod_3$DataSet<-names(dat_mod_1)

topts_cpm_3<-merge(vcmax.1,topt_mod_3,by="DataSet")
topts_cpm_3$PFT[topts_cpm_3$DataSet=="SAVANNA"] <- "BET_TE" #change PFT of SAVANNA back to BET_TE
topts_cpm_3$num <- ave( topts_cpm_3$Topt_275, factor(topts_cpm_3$PFT),FUN = seq_along) # set  unique numbers for colors

# 
# windows(60,60);
# par(cex.lab=1.5,mar=c(5,5,5,5),oma=c(5,5,5,5),cex.axis=1.5,las=1,mfrow=c(1,1))

# pdf(file="Figure2-Photo_vs_Temperature.pdf",width=4,height=4)
# par(mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,0))

#with(t_home.1,plot(MAXSS,Topt,col=COL[factor(DataSet)],pch=16,cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,40),ylim=c(10,35)))

with(topts_cpm_3,plot(Topt_275,topts,bg=COL[PFT],pch=c(21,22,23,24,25)[topts_cpm_3$num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(15,35),ylim=c(15,35)))
adderrorbars(x=topts_cpm_3$Topt_275,y=topts_cpm_3$topts,SE=topts_cpm_3$Topt_275.se ,direction="updown")
# ae<-subset(t_home.1,t_home.1$Topt.se<8) #remove one Se=9.4 to improve the clarity
# adderrorbars(x=topts_cpm_1$Topt_275,y=topts_cpm_1$topts,SE=topts_cpm_1$Topt_275.se ,direction="leftright")

#adderrorbars(x=t_home.1$MAXSS,y=t_home.1$Topt,SE=ae$Topt.se ,direction="updown")

magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T)


#legend("topleft",c("BDT-Te","BET-Te","NET-Br","NET-Te","BET-Tr","Arctic"),col =COL,pch=16,ncol=2,cex=.8,pt.cex=1,pt.lwd=1,title="PFT",bty="n")
abline(a=0,b=1)
legend("topright",expression(bold(c)),bty="n",cex=1.5)

title(ylab=expression(Topt[predicted]~(degree*C)),
      xpd=NA,cex.lab=2)
title(xlab=expression(Topt[measured]~(degree*C)),
      xpd=NA,cex.lab=2,adj=.3)

summary(lm(topts~Topt_275,data=subset(topts_cpm_3,!topts_cpm_3$DataSet %in% c("Hikosaka","WANG_ET_AL","HAN_2"))))
ablineclip(a=3.38943,b=0.88429,x1=16,x2=34,y1=17,y2=34,lty=2,col="red",lwd=2)

#----------------------------------------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------------------------------------
