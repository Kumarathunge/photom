#---------------------------------------------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------------------------------------------
# Plot parameters of temperature response of Vcmax and Jmax (mature trees in native environments)
# Manuscript Figure 2 
#--------------------------------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------------------------------


# Read biochemical data (fitted parameters)

tfits_VJ<-read.csv(paste0(path,"/Tables/biochemical_parameters_with_met_data_with_rd.csv"))
tfits_VJ$Tavg_30<-ifelse(is.na(tfits_VJ$Tavg_30),tfits_VJ$gMDD0,tfits_VJ$Tavg_30)

t_home_bio<-subset(tfits_VJ,  DataSet %in% c("GWW","RF_AMZ","SAVANNA","TARVAINEN","TMB",
                                             "WANG_ET_AL","RF_AUS","HAN_2","Martijn_Slot","ARCTIC","EucFace","ANNA","Alida Mao","TARVAINEN_B",
                                             "Kelsey_Cater","Hikosaka","ELLSWORTH","MEDLYN")) 

t_home_bio<-subset(t_home_bio,!t_home_bio$Species %in% c("Pinus pinaster_T","Betula pendula") 
                   & !t_home_bio$Season %in% c("WINTER","winter","dry",4,10,"June","August","Oct",
                                               "Jan","Jul","Mar","Nov","Jun"))

t_home_bio$DataSet<-factor(t_home_bio$DataSet)

t_home_bio<-t_home_bio[with(t_home_bio, order(PFT, DataSet)), ]

t_home_bio$num <- ave( t_home_bio$Vcmax25, t_home_bio$PFT,FUN = seq_along) # set  unique numbers for colors

t_home_bio$Topt_jmax<-ifelse(is.na(t_home_bio$Topt_jmax),t_home_bio$ToptJ,t_home_bio$Topt_jmax)

t_home_bio$delsv.new<-t_home_bio$delsV*1000
t_home_bio$delsj.new<-t_home_bio$delsJ*1000


COL<-c("#E41A1C" ,"#377EB8", "#4DAF4A", "#984EA3", "#FF7F00", "#8DD3C7")

# png(filename="manuscript_figures/figure_2.png",width = 500, height = 500*1.4)

# par(cex.lab=1.5,mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,5),cex.axis=1.5,las=1,mfrow=c(4,2))

pdf("manuscript_figures/new_figures/figure_2.pdf",width = 5, height = 6)
par(cex.lab=1.5,mar=c(0.5,0.5,0.5,0.5),oma=c(5,5,0,5),cex.axis=1.5,las=1,mfrow=c(3,2))

# 1. Plot Vcmax at 25C

# with(t_home_bio,plot(gMDD0,Vcmax25,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=2,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(10,200)))
# adderrorbars(x=t_home_bio$gMDD0,y=t_home_bio$Vcmax25,SE=t_home_bio$Vcmax25.se ,direction="updown")
# magaxis(side=c(1,2,4),labels=c(0,1,0),frame.plot=T)
# title(ylab=expression(V[cmax]~(mu*mol~m^-2~s^-1)),
#       xpd=NA,cex.lab=1.6)
# 
# legend("topright",expression((bold(a))),bty="n",cex=1.5)


#--

# Plot Jmax at 25C

# with(t_home_bio,plot(gMDD0,Jmax25,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=2,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(10,200)))
# 
# adderrorbars(x=t_home_bio$gMDD0,y=t_home_bio$Jmax25,SE=t_home_bio$Jmax25.se ,direction="updown")
# magaxis(side=c(1,2,4),labels=c(0,0,1),frame.plot=T)
# title(ylab=expression(J[max]~(mu*mol~m^-2~s^-1)),
#       xpd=NA,cex.lab=1.6,line=-25)
# plot_fit_line(data=t_home_bio,yvar="Jmax25",xvar="gMDD0",linecol="black",fitoneline=T,lwd=2,lty=1)
# legend("topright",expression((bold(b))),bty="n",cex=1.5)

#--
#-----------------

#- Topt for Vcmax

with(t_home_bio,plot(Tavg_30,Topt_vcmax_PA,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=.2,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(20,60)))
magaxis(side=c(1,2,4),labels=c(0,1,0),frame.plot=T)
title(ylab=expression(Topt[V]~(degree*C)),
      xpd=NA,cex.lab=1.6)


adderrorbars(x=t_home_bio$Tavg_30,
             y=t_home_bio$Topt_vcmax_PA,
             SE=t_home_bio$Topt_vcmax_SE_PA ,direction="updown")

with(t_home_bio,points(Tavg_30,Topt_vcmax_PA,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(20,60)))


plot_fit_line(t_home_bio,yvar="Topt_vcmax_PA",xvar="Tavg_30",linecol="black",fitoneline=T,lwd=2,lty=1)

legend("topright",expression((bold(a))),bty="n",cex=1.5)
# legend("bottomright",expression(R^2==0.67),bty="n")


#--------------------------
#- add Kattge & Knorr model to the plot
ablineclip(a=24.92,b=0.44,x1=min(t_home_bio$Tavg_30)-3,x2=max(t_home_bio$Tavg_30)+3,lty = 3,lwd=2)

#--------------------------



#- Topt for Jmax

with(t_home_bio,plot(Tavg_30,Topt_jmax_PA,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=.2,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(20,60)))


magaxis(side=c(1,2,4),labels=c(0,0,1),frame.plot=T)
title(ylab=expression(Topt[J]~(degree*C)),
      xpd=NA,cex.lab=1.6,line=-18)

ae<-subset(t_home_bio,t_home_bio$Topt_Jmax_SE_PA<15) #remove two SEs to improve the clarity

adderrorbars(x=ae$Tavg_30,
             y=ae$Topt_jmax,
             SE=ae$Topt_Jmax_SE_PA,direction="updown")

with(t_home_bio,points(Tavg_30,Topt_jmax_PA,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(20,60)))


plot_fit_line(t_home_bio,yvar="Topt_jmax",xvar="Tavg_30",linecol="black",fitoneline=T,lwd=2,lty=1)

legend("topright",expression((bold(d))),bty="n",cex=1.5)
# legend("bottomright",expression(R^2==0.47),bty="n")

#--------------------------
#- add Kattge & Knorr model to the plot
ablineclip(a=26.21,b=0.33,x1=min(t_home_bio$Tavg_30)-3,x2=max(t_home_bio$Tavg_30)+3,lty = 3,lwd=2)


#--------------------------

# Plot Ea of Vcmax


with(t_home_bio,plot(Tavg_30,EaV,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=.2,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(0,150)))
adderrorbars(x=t_home_bio$Tavg_30,y=t_home_bio$EaV,SE=t_home_bio$EaV.se ,direction="updown")
magaxis(side=c(1,2,4),labels=c(0,1,0),frame.plot=T)
title(ylab=expression(E[a(V)]~(kJ~mol^-1)),
      xpd=NA,cex.lab=1.6)
plot_fit_line(data=t_home_bio,yvar="EaV",xvar="Tavg_30",linecol="black",fitoneline=T,lwd=2,lty=1)
legend("topright",expression((bold(b))),bty="n",cex=1.5)

with(t_home_bio,points(Tavg_30,EaV,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(0,150)))

#-- 
# Plot Ea of Jmax

with(t_home_bio,plot(Tavg_30,EaJ,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=.2,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(0,150)))
adderrorbars(x=t_home_bio$Tavg_30,y=t_home_bio$EaJ,SE=t_home_bio$EaJ.se ,direction="updown")
magaxis(side=c(1,2,4),labels=c(0,0,1),frame.plot=T)


title(ylab=expression(E[a(J)]~(kJ~mol^-1)),
      xpd=NA,cex.lab=1.6,line=-18)
legend("topright",expression((bold(e))),bty="n",cex=1.5)

with(t_home_bio,points(Tavg_30,EaJ,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(0,150)))

#--

# Plot deltaS parameters 
# deltaS of Vcmax 

with(t_home_bio,plot(Tavg_30,delsV*1000,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=.2,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(610,680)))

ae.1<-subset(t_home_bio,t_home_bio$delsV.se<.03) #remove two SEs to improve the clarity

adderrorbars(x=t_home_bio$Tavg_30,
             y=t_home_bio$delsV*1000,
             SE=ae.1$delsV.se*1000 ,direction="updown")

magaxis(side=c(1,2,4),labels=c(1,1,0),frame.plot=T)
title(ylab=expression(Delta*S[V]~(J~mol^-1~K^-1)),
      xpd=NA,cex.lab=1.6)

with(t_home_bio,points(Tavg_30,delsV*1000,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(610,680)))


plot_fit_line(t_home_bio,yvar="delsv.new",xvar="Tavg_30",linecol="black",fitoneline=T,lwd=2,lty=1)
legend("topright",expression((bold(c))),bty="n",cex=1.5)
# legend("bottomright",expression(R^2==0.55),bty="n")

# lm_delsv<-lm(delsv.new~Tavg_30,data=t_home_bio)
# summary(lm_delsv)

#--------------------------
#- add Kattge & Knorr model to the plot
ablineclip(a=668.39,b=-1.07,x1=min(t_home_bio$Tavg_30)-3,x2=max(t_home_bio$Tavg_30)+3,lty = 3,lwd=2)

#--------------------------

#-----------
#- dels Jmax
with(t_home_bio,plot(Tavg_30,delsJ*1000,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=.2,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(610,680)))

ae<-subset(t_home_bio,t_home_bio$delsJ.se<.04) #remove two SEs to improve the clarity
adderrorbars(x=t_home_bio$Tavg_30,y=t_home_bio$delsJ*1000,SE=ae$delsJ.se*1000 ,direction="updown")

magaxis(side=c(1,2,4),labels=c(1,0,1),frame.plot=T)
title(ylab=expression(Delta*S[J]~(J~mol^-1~K^-1)),
      xpd=NA,cex.lab=1.6,line=-18)

plot_fit_line(t_home_bio,yvar="delsv.new",xvar="Tavg_30",linecol="black",fitoneline=T,lwd=2,lty=1)
legend("topright",expression((bold(f))),bty="n",cex=1.5)

with(t_home_bio,points(Tavg_30,delsJ*1000,bg=COL[PFT],pch=c(21,22,23,24,25)[num],cex=1.5,ylab="",xlab="",axes=F,xlim=c(0,35),ylim=c(610,680)))

#--------------------------
#- add Kattge & Knorr model to the plot
ablineclip(a=659.70,b=-0.75,x1=min(t_home_bio$Tavg_30)-3,x2=max(t_home_bio$Tavg_30)+3,lty = 3,lwd=2)

#--------------------------




title(xlab=expression(~T[growth]~(degree*C)),cex.lab=1.6,outer=T,adj=0.19,line=2)
title(xlab=expression(~T[growth]~(degree*C)),cex.lab=1.6,outer=T,adj=0.85,line=2)




dev.off()

#--------------
#--------------

