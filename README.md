This repository containing the R code to data analysis and reproduce all the figures and tables of the manuscript titled 
“Acclimation and adaptation components of the temperature dependence of plant photosynthesis at the global scale” by Kumarathunge and Medlyn et al.

The code within "centralscript.R" will download all of the raw data, place them in the "Data" folder, 
and recreate all of the figures and tables in the “manuscript_figures” and “manuscript_tables” folders. 

Send Dushan an e-mail if you have trouble, and he will try to help you out (d.kumarathunge@westernsydney.edu.au). 
